# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


from openerp import models, fields, api, _
from openerp.osv import fields, osv


class account_notification_period(osv.osv): 
    _name = 'account.notification.period'
    _description = 'Invoice Notification Period'
    
    _columns = {
        'name': fields.integer('Notification period', translate=True),
        'default': fields.boolean('Default')
        }

account_notification_period()


class account_interest_arrears(osv.osv): 
    _name = 'account.interest.arrears'
    _description = 'Invoice Interest Arrears'
    
    _columns = {
        'name': fields.float('Interest on arrears', translate=True),
        'default': fields.boolean('Default')
        }

account_interest_arrears()


class account_invoice(osv.osv): 
    _inherit = 'account.invoice'
    _description = 'Account Invoice'
    
    _columns = {
        'x_notification_period': fields.many2one('account.notification.period', 'Notification period', translate=True),
        'x_interest_arrears': fields.many2one('account.interest.arrears', 'Interest on arrears', translate=True),
        }
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(account_invoice, self).default_get(cr, uid, fields, context=context)
        notification_pool = self.pool.get('account.notification.period')
        interest_arrears_pool = self.pool.get('account.interest.arrears')
        notif_id = notification_pool.search(cr, uid, [('default', '=', True)], context=context)
        interest_id = interest_arrears_pool.search(cr, uid, [('default', '=', True)], context=context)
        if notif_id:
            res.update({'x_notification_period': notif_id[0]})
        if interest_id:
            res.update({'x_interest_arrears': interest_id[0]})
             
        return res

    @api.multi
    def invoice_print(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'sprintit_invoice_report.report_invoice')
    
account_invoice()


class account_invoice_line(osv.osv): 
    _inherit = 'account.invoice.line'
    _description = 'Account Invoice Line'
      
    _columns = {
        'delivery_date': fields.date('Delivery Date', translate=True)
        }

account_invoice_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
