# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
{
    'name': 'Sprintit Invoice Report',
    'version': '1.3',
    'category': 'Sprintit Invoice Report',
    'description': """Invoice Report""",
    'author': 'SprintIT, Roy Nurmi',
    'maintainer': 'SprintIT, Roy Nurmi',
    'website': 'http://www.sprintit.fi',
    'images': [],
    'depends': ['sprintit_mbo_customization', 'account_invoice_shipping_address'],
    'data': [
        'security/ir.model.access.csv',
        'account_invoice_view.xml',
        'account_report.xml',
        'views/report_invoice.xml',
        ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: