# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv

class sale_order_line(osv.osv): 
    _inherit = 'sale.order.line'
    
    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        res = super(sale_order_line,self)._prepare_order_line_invoice_line(cr, uid, line, account_id, context)
        res.update({'delivery_date': line.order_id and line.order_id.date_order or ''})
        return res
  
sale_order_line()


class sale_advance_payment_inv(osv.osv_memory):
    _inherit = "sale.advance.payment.inv"
    
    def _prepare_advance_invoice_vals(self, cr, uid, ids, context=None):
        res = super(sale_advance_payment_inv,self)._prepare_advance_invoice_vals(cr, uid, ids, context)
        sale_obj = self.pool.get('sale.order')
        for item in res:
            sale = sale_obj.browse(cr, uid, item[0], context=context)
            item[1]['invoice_line'][0][2].update({'delivery_date': sale.date_order})
        return res
    
sale_advance_payment_inv()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
