# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

import account_invoice
import sale

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: